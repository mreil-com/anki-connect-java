package com.mreil.ankiconnectjava.util;

import com.google.common.reflect.ClassPath;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class ResourceReader {
    private final String rootDir;

    public ResourceReader(String rootDir) {
        this.rootDir = rootDir;
    }

    public List<ClassPath.ResourceInfo> getAllResources() throws IOException {
        ClassPath cp = ClassPath.from(this.getClass().getClassLoader());

        List<ClassPath.ResourceInfo> allResources = cp.getResources()
                .asList().stream()
                .filter(r -> r.getResourceName().startsWith(rootDir))
                .collect(Collectors.toList());
        return allResources;
    }
}
