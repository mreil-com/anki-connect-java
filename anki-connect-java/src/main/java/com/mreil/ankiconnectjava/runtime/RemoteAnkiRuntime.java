package com.mreil.ankiconnectjava.runtime;

import com.mreil.ankiconnectjava.AnkiClient;

public class RemoteAnkiRuntime extends AbstractAnkiRuntime {
    RemoteAnkiRuntime(AnkiClient client) {
        super(client);
    }

    @Override
    public void shutdown() {
        // do nothing
    }
}
