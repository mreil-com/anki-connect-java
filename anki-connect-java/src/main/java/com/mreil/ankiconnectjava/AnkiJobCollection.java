package com.mreil.ankiconnectjava;

import java.io.IOException;
import java.util.Collection;

public interface AnkiJobCollection {
    Collection<AnkiJob> getJobs() throws IOException;
}
