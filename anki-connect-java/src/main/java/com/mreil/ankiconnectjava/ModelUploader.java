package com.mreil.ankiconnectjava;

import com.mreil.ankiconnectjava.model.CreateModelAction;
import com.mreil.ankiconnectjava.model.ImmutableCreateModelAction;
import com.mreil.ankiconnectjava.model.internal.DeckData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class ModelUploader {
    private final Logger log = LoggerFactory.getLogger(ModelUploader.class);
    private final AnkiClient client;
    private final Path modelDirectory;
    private final DeckData deckData;

    public ModelUploader(AnkiClient client, Path modelDirectory, DeckData deckData) {
        this.client = client;
        this.modelDirectory = modelDirectory;
        this.deckData = deckData;
    }

    public String upload() throws IOException {
        String modelName = modelDirectory.getFileName().toString();
        log.info("Uploading model '{}'...", modelName);

        String css = readFile("styles.css");
        String front = readFile("Front.html");
        String back = readFile("Back.html");

        CreateModelAction createModelAction = ImmutableCreateModelAction.builder()
                .params(ImmutableCreateModelAction.Params.builder()
                        .addCardTemplates(ImmutableCreateModelAction.CardTemplates.builder()
                                .front(front)
                                .back(back)
                                .build())
                        .modelName(modelName)
                        .inOrderFields(deckData.getFieldNames())
                        .css(css)
                        .build())
                .build();
        client.createModel(createModelAction);
        return modelName;
    }

    private String readFile(String other) throws IOException {
        File file = new ClassPathResource(modelDirectory.resolve(other).toString()).getFile();
        return String.join("\n", Files.readAllLines(file.toPath()));
    }
}
