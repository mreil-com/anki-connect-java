package com.mreil.ankiconnectjava.model;

import org.immutables.value.Value;

@Value.Immutable
public interface ExitAnkiAction extends Action {

    public static final ExitAnkiAction EXIT_ACTION = ImmutableExitAnkiAction.builder().build();

    @Override
    default String getAction() {
        return "guiExitAnki";
    }
}
