package com.mreil.ankiconnectjava.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

@Value.Immutable
@Value.Enclosing
@Value.Style(get = {"get*", "is*"})
@JsonDeserialize(builder = ImmutableDeckConfig.Builder.class)
public interface DeckConfig {
    @Value.Default
    default New getNew() {
        return ImmutableDeckConfig.New.builder().build();
    }

    @Value.Default
    default Lapse getLapse() {
        return ImmutableDeckConfig.Lapse.builder().build();
    }

    @Value.Default
    default Rev getRev() {
        return ImmutableDeckConfig.Rev.builder().build();
    }

    /**
     * ?
     */
    default boolean dyn() {
        return false;
    }

    /**
     * Automatically play audio
     */
    @Value.Default
    default boolean autoplay() {
        return true;
    }

    /**
     * ?
     */
    default long getMod() {
        return System.currentTimeMillis() / 1000;
    }

    /**
     * Id of the deck configuration.
     * Get value from getDeckConfig info.
     * By default, set default id "1".
     */
    @Value.Default
    default int getId() {
        return 1;
    }

    /**
     * Ignore answer times longer than (seconds).
     */
    @Value.Default
    default int getMaxTaken() {
        return 60;
    }

    /**
     * change "Default" settings.
     */
    @Value.Default
    default String getName() {
        return "Default";
    }

    /**
     * 0 = Don't show timer
     * 1 = Show timer
     */
    @Value.Default
    default int getTimer() {
        return 0;
    }

    /**
     * Always include question side when replaying audio
     */
    @Value.Default
    default boolean replayq() {
        return true;
    }

    /**
     * ?
     */
    default int getUsn() {
        return -1;
    }

    /**
     * <pre>
     * "lapse": {
     *     "leechFails": 8,
     *     "delays": [10],
     *     "minInt": 1,
     *     "leechAction": 0,
     *     "mult": 0
     * }
     * </pre>
     */
    @Value.Immutable
    @JsonDeserialize(builder = ImmutableDeckConfig.Lapse.Builder.class)
    interface Lapse {
        /**
         * Leech threshold (lapses)
         */
        @Value.Default
        default int getLeechFails() {
            return 8;
        }

        /**
         * i0 = Steps in minutes
         */
        @Value.Default
        default int[] getDelays() {
            return new int[]{10};
        }

        /**
         * Minimum interval (days)
         */
        @Value.Default
        default int getMinInt() {
            return 1;
        }

        /**
         * 0 = suspend card
         * 1 = tag only
         */
        @Value.Default
        default int getLeechAction() {
            return 0;
        }

        /**
         * New interval
         */
        @Value.Default
        default int getMult() {
            return 0;
        }
    }

    /**
     * <pre>
     * "new": {
     *    "bury": true,
     *    "order": 1,
     *    "initialFactor": 2500,
     *    "perDay": 20,
     *    "delays": [1, 10],
     *    "separate": true,
     *    "ints": [1, 4, 7]
     * },
     * </pre>
     */
    @Value.Immutable
    @JsonDeserialize(builder = ImmutableDeckConfig.New.Builder.class)
    interface New {
        @Value.Default
        default boolean isBury() {
            return false;
        }

        /**
         * 0 = random order
         * 1 = order added
         */
        @Value.Default
        default int getOrder() {
            return 0;
        }

        @Value.Default
        default int getInitialFactor() {
            return 2500;
        }

        @Value.Default
        default int getPerDay() {
            return 9999;
        }

        @Value.Default
        default int[] getDelays() {
            return new int[]{1, 10};
        }

        /**
         * Graduating interval, Easy interval, ?
         */
        @Value.Default
        default int[] getInts() {
            return new int[]{1, 4, 7};
        }

        @Value.Default
        default boolean separate() {
            return false;
        }
    }

    /**
     * <pre>
     * "rev": {
     *    "bury": true,
     *    "ivlFct": 1,
     *    "ease4": 1.3,
     *    "maxIvl": 36500,
     *    "perDay": 100,
     *    "minSpace": 1,
     *    "fuzz": 0.05
     * },
     * </pre>
     */
    @Value.Immutable
    @JsonDeserialize(builder = ImmutableDeckConfig.Rev.Builder.class)
    interface Rev {

        @Value.Default
        default boolean bury() {
            return false;
        }

        @Value.Default
        default int getPerDay() {
            return 9999;
        }

        /**
         * Easy bonus
         */
        @Value.Default
        default float getEase4() {
            return 1.3f;
        }

        /**
         * Interval modifier
         */
        @Value.Default
        default float getIvlFct() {
            return 1.f;
        }

        /**
         * Maximum interval
         */
        @Value.Default
        default int getMaxIvl() {
            return 36500;
        }

        @Value.Default
        default int getMinSpace() {
            return 1;
        }

        @Value.Default
        default float getFuzz() {
            return 0.05f;
        }
    }
}
