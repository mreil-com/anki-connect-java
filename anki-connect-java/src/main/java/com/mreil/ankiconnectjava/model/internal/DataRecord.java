package com.mreil.ankiconnectjava.model.internal;

import org.springframework.util.StringUtils;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DataRecord {
    public static final String TAGS = "tags";

    private final Map<String, ? extends String> fields;
    private final Set<String> tags;
    private final Set<String> mediaFileNames;

    public DataRecord(Map<String, String> fields) {
        this.tags = new HashSet<>(Arrays.asList(fields
                .getOrDefault(TAGS, "")
                .split("\\s*;\\s")));
        this.mediaFileNames = fields.entrySet().stream()
                .filter(e -> e.getKey().startsWith("data_"))
                .map(Map.Entry::getValue)
                .filter(val -> !StringUtils.isEmpty(val))
                .collect(Collectors.toSet());
        this.fields = filter(fields);
    }

    public Map<String, ? extends String> getFields() {
        return fields;
    }

    public Set<String> getTags() {
        return tags;
    }

    public Set<String> getMediaFileNames() {
        return mediaFileNames;
    }

    private Map<String, String> filter(Map<String, String> fields) {
        return fields.entrySet().stream()
                .map(this::filterAudio)
                .map(this::filterImage)
                .map(this::filterTags)
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private Map.Entry<String, String> filterAudio(Map.Entry<String, String> entry) {
        return filter(entry, "data_audio", this::newSoundEntry);
    }

    private Map.Entry<String, String> filterImage(Map.Entry<String, String> entry) {
        return filter(entry, "data_image", this::newImageEntry);
    }

    private Map.Entry<String, String> filterTags(Map.Entry<String, String> entry) {
        return filter(entry, TAGS, e -> null);
    }

    private Map.Entry<String, String> newSoundEntry(Map.Entry<String, String> e) {
        return new AbstractMap.SimpleEntry<>(e.getKey(), "[sound:" + e.getValue() + "]");
    }

    private Map.Entry<String, String> newImageEntry(Map.Entry<String, String> e) {
        return new AbstractMap.SimpleEntry<>(e.getKey(), "<img src=\"" + e.getValue() + "\"/>");
    }

    private Map.Entry<String, String> filter(Map.Entry<String, String> entry,
                                             String key,
                                             Function<Map.Entry<String, String>, Map.Entry<String, String>> replacement) {
        return Stream.of(entry)
                .filter(e -> e.getKey().equals(key))
                .filter(e -> !e.getValue().isEmpty())
                .map(replacement)
                .filter(Objects::nonNull)
                .findFirst()
                .orElse(entry);
    }
}
